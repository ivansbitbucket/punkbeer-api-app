"use strict";

let content = document.getElementById('showContent'),
    contentContainer = document.getElementById('thecontent'),
    htmlString,
    beerName = document.querySelector('.beerName'),
    loader = document.querySelectorAll('.loader'),
    beerPlaceHolder = document.querySelector('.beerPlaceHolder');

content.addEventListener('click', (e) => {
    let randomNumber = Math.floor((Math.random()*25)+1),
        ourRequest = new XMLHttpRequest(),
        ourData = null;
    ourRequest.open("GET",'https://api.punkapi.com/v2/beers/' + randomNumber);
    ourRequest.onload = () =>{
        try{
            ourData = JSON.parse(ourRequest.responseText);
            renderHTML(ourData);
       } catch(e) {
            ourData = ourRequest.responseText;
       }
    };
    content.textContent ="Click one more time to see your BEERS!";
    loader[0].style.display="none";
    ourRequest.send();
    e.stopPropagation();
});
function renderHTML(data){
    contentContainer.innerHTML = htmlString;
    for ( let i = 0; i < data.length; i++) {
        htmlString =`
        <div class="beerContent">
        <div class="beerImg">
        <img src="${data[0].image_url}" alt=""/>
        </div>
        <div class="beerText">
        <h2>${data[0].name}</h2>
        <p>${data[0].tagline}</p>
        <p class="description">${data[0].description}</p>
        <p>${data[0].brewers_tips}</p>
        <ul>
            <li><span>ibu</span> ${data[0].ibu}</li>
            <li class="abv"><span>abv</span>  ${data[0].abv}  % </li>
            <li><span>ebc</span> ${data[0].ebc}</li>
        </ul>
        <h3>Best with:</h3>
        <div id="foodDivs"></div>
        <p class="foods">${data[0].food_pairing}</p>
        </div>
        `
       ;
       let foods = document.querySelector('.foods'),
           foodsText = foods.innerHTML,
           foodsArray = foodsText.split(','),
           foodDivs = document.getElementById('foodDivs');
        for(let i = 0; i < foodsArray.length; i++) {
            foodDivs.innerHTML += "<ul><li>" + foodsArray[i] + "</li></ul>";
        }
    };
};


